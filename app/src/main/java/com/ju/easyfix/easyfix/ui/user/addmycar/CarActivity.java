package com.ju.easyfix.easyfix.ui.user.addmycar;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.admin.CarType;
import com.ju.easyfix.easyfix.model.user.MyCars;
import com.ju.easyfix.easyfix.ui.admin.car.model.adapter.SpCarModelAdapter;
import com.ju.easyfix.easyfix.ui.admin.car.model.adapter.SpCarTypeAdapter;
import com.ju.easyfix.easyfix.util.SessionUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ju.easyfix.easyfix.constant.FirebaseConstant.COMPANY_TABLE;
import static com.ju.easyfix.easyfix.constant.FirebaseConstant.MY_CAR_TABLE;
import static com.ju.easyfix.easyfix.constant.FirebaseConstant.USERS_TABLE;


public class CarActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    @BindView(R.id.sp_car_type)
    AppCompatSpinner spCarType;
    @BindView(R.id.sp_car_model)
    AppCompatSpinner spCarModel;

    @BindView(R.id.et_plaint_number)
    AppCompatEditText etPlaintNumber;
    @BindView(R.id.iv_car_image)
    AppCompatImageView ivCarImage;
    @BindView(R.id.et_car_year)
    AppCompatEditText etCarYear;
    private DatabaseReference databaseCompany;
    private DatabaseReference databaseUser;
    List<CarType> carTypeList = new ArrayList<>();
    List<MyCars> myCarsList = new ArrayList<>();
    private Context context = this;
    private DatabaseReference databaseReference;
    private int idCarTypeSelected;
    private int idCarModelSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_add_car);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
        init();
        getCarType();

    }

    private void init() {
        databaseCompany = FirebaseDatabase.getInstance().getReference().child(COMPANY_TABLE);
        spCarType.setOnItemSelectedListener(this);
        spCarModel.setOnItemSelectedListener(this);
    }


    private void getCarType() {

        databaseReference = FirebaseDatabase.getInstance().getReference().child(COMPANY_TABLE);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                carTypeList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    CarType carType = child.getValue(CarType.class);
                    carType.setId(child.getKey());
                    if (carType.getCarModels() != null && !carType.getCarModels().isEmpty()) {
                        carTypeList.add(carType);
                    }
                }
                spCarType.setAdapter(new SpCarTypeAdapter(context, carTypeList));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.sp_car_type:
                idCarTypeSelected = position;
                spCarModel.setAdapter(new SpCarModelAdapter(context, carTypeList.get(position).getCarModels()));

                break;
            case R.id.sp_car_model:
                idCarModelSelected = position;

                Picasso.with(context).load(carTypeList.get(idCarTypeSelected).getCarModels().get(position).getImage()).into(ivCarImage);

                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @OnClick(R.id.btn_add_car)
    public void onViewClicked() {
        databaseUser = FirebaseDatabase.getInstance().getReference().child(USERS_TABLE)
                .child(SessionUtils.getInstance().mAuth().getUid()).child(MY_CAR_TABLE);

        databaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                myCarsList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    MyCars myCar = child.getValue(MyCars.class);
                    if (myCar.getIdCarType() != null) {
                        myCarsList.add(myCar);
                    }
                }
                addMyCar();

                Toast.makeText(context, "Added", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void addMyCar() {
        MyCars myCar = new MyCars();
        myCar.setId(myCarsList.size() + "");
        myCar.setIdCarType(idCarTypeSelected + "");
        myCar.setIdCarModel(idCarModelSelected + "");
        myCar.setType(carTypeList.get(idCarTypeSelected).getName() + "");
        myCar.setModel(carTypeList.get(idCarTypeSelected).getCarModels().get(idCarModelSelected).getName());
        myCar.setImageBackground(carTypeList.get(idCarTypeSelected).getCarModels().get(idCarModelSelected).getImageBackground());
        myCar.setImage(carTypeList.get(idCarTypeSelected).getCarModels().get(idCarModelSelected).getImage());

        myCar.setYear(etCarYear.getText().toString());
        myCar.setPlateNumber(etPlaintNumber.getText().toString());

        myCarsList.add(myCar);

        databaseUser.setValue(myCarsList);

    }
}
