package com.ju.easyfix.easyfix.ui.user.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.listener.IReserveListener;

import java.util.List;

public class ReservationTimeAdapter extends RecyclerView.Adapter<ReservationTimeAdapter.ViewHolder> {
    private static final int ROW_RESERVATION_TIME = R.layout.row_reservation_time;
    private List<String> reservationTimeList;
    private Integer lastCheckedPosition = 0;
    private IReserveListener reserveListener;

    public ReservationTimeAdapter(List<String> reservationTimeList,
                                  IReserveListener reserveListener) {
        this.reservationTimeList = reservationTimeList;
        this.reserveListener = reserveListener;
        reserveListener.getReservationTimeChosen(reservationTimeList.get(0));
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType,
                parent, false);
        return new ReservationTimeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.checkboxTime.setText(reservationTimeList.get(position));


        holder.checkboxTime.setChecked(position == lastCheckedPosition);
        if (position == lastCheckedPosition) {
            return;
        }
        holder.checkboxTime.setOnClickListener(v -> {
            setLastCheckedPosition(holder);
            reserveListener.getReservationTimeChosen(reservationTimeList.get(position));
        });

    }

    private void setLastCheckedPosition(ViewHolder holder) {
        lastCheckedPosition = holder.getAdapterPosition();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return reservationTimeList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return ROW_RESERVATION_TIME;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CheckBox checkboxTime;

        public ViewHolder(View itemView) {
            super(itemView);
            checkboxTime = itemView.findViewById(R.id.cb_reservation_time);
        }
    }
}
