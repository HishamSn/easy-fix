package com.ju.easyfix.easyfix.ui.user.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.explib.ECCardData;
import com.ju.easyfix.easyfix.explib.ECPagerView;
import com.ju.easyfix.easyfix.explib.ECPagerViewAdapter;
import com.ju.easyfix.easyfix.model.CarCard;
import com.ju.easyfix.easyfix.model.user.MyCars;
import com.ju.easyfix.easyfix.ui.base.BaseActivity;
import com.ju.easyfix.easyfix.ui.user.addmycar.CarActivity;
import com.ju.easyfix.easyfix.util.SessionUtils;
import com.ju.easyfix.easyfix.util.Util;
import com.ju.easyfix.easyfix.view.ItemsCountView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ju.easyfix.easyfix.constant.FirebaseConstant.MY_CAR_TABLE;
import static com.ju.easyfix.easyfix.constant.FirebaseConstant.USERS_TABLE;
import static com.ju.easyfix.easyfix.util.LocationUtil.isGPSEnable;
import static com.ju.easyfix.easyfix.util.LocationUtil.showDialogEnableLocation;


@SuppressLint("SetTextI18n")
public class MainActivity extends BaseActivity {

    @BindView(R.id.fab_add_car)
    FloatingActionButton fabAddCar;
    @BindView(R.id.ec_pager_element)
    ECPagerView ecPagerElement;

    private ProgressDialog progress;

    private DatabaseReference databaseReference;

    private List<MyCars> myCarsList;
    private Context context = this;
    private boolean isViewPagerCollapse = false;
    private ReserveArrayAdapter reserveArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();

        initNavigation();
        getMyCars();
//        getLocation();

//        checkLocation();
    }

    private void checkLocation() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Log.e("locationh", "Manifest.permission.ACCESS_FINE_LOCATION = true");
            // User denied location
        } else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageInfo.REQUESTED_PERMISSION_GRANTED) {
            // Request permission
            Log.e("locationh", "Manifest.permission.ACCESS_FINE_LOCATION = false");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        } else {
            // User allow location permission
            Log.e("locationh", "else location = false");
        }
    }


    private void init() {
        myCarsList = new ArrayList<>();
        progress = Util.progressUtil(this);

    }

    private List<ECCardData> getDataset() {
        List<ECCardData> dataset = new ArrayList<>();

        for (int i = 0; i < myCarsList.size(); i++) {
            CarCard car = new CarCard();

            car.setMainBackgroundResource(R.drawable.car_workshop);
            car.setIdCarType(myCarsList.get(i).getIdCarType());
            car.setIdMyCar(myCarsList.get(i).getId());
            car.setUrlImage(myCarsList.get(i).getImage());
            car.setHeadTitle(myCarsList.get(i).getType());
            car.setCarType(myCarsList.get(i).getType());
            car.setCarModel(myCarsList.get(i).getModel());
            car.setYear(myCarsList.get(i).getYear());
            car.setPlateNumber(myCarsList.get(i).getPlateNumber());
            dataset.add(car);
        }
        return dataset;
    }

    private void getMyCars() {
        progress.show();
        databaseReference = FirebaseDatabase.getInstance().getReference()
                .child(USERS_TABLE).child(SessionUtils.getInstance().mAuth().getUid()).child(MY_CAR_TABLE);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progress.dismiss();
                myCarsList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    MyCars myCar = child.getValue(MyCars.class);
                    if (myCar != null) {
                        myCarsList.add(myCar);
                    }
                }
                setViewPager();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progress.dismiss();

            }
        });
    }


    private void setViewPager() {
        // Create adapter for ViewPager
        CustomEcPagerViewAdapter customEcPagerViewAdapter;
        customEcPagerViewAdapter = new CustomEcPagerViewAdapter(this, getDataset());

        ecPagerElement.setPagerViewAdapter(customEcPagerViewAdapter);
        ecPagerElement.setBackgroundSwitcherView(findViewById(R.id.ec_bg_switcher_element));

        final ItemsCountView itemsCountView = findViewById(R.id.items_count_view);
        ecPagerElement.setOnCardSelectedListener((newPosition, oldPosition, totalElements) ->
                itemsCountView.update(newPosition, oldPosition, totalElements));
    }

    private void initNavigation() {
        setNavigation(
                findViewById(R.id.nv_main),
                findViewById(R.id.toolbar),
                findViewById(R.id.cl_parent)
        );
    }

    private void configureHeadCard(LayoutInflater inflaterService, ViewGroup headCard, CarCard carCard) {
        View gradient = new View(getApplicationContext());
        gradient.setLayoutParams(
                new FrameLayout.LayoutParams
                        (FrameLayout.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.MATCH_PARENT));
        gradient.setBackgroundDrawable(getResources().getDrawable(R.drawable.card_head_gradient));
        headCard.addView(gradient);

        // Inflate main header layout and attach it to header root view
        inflaterService.inflate(R.layout.simple_head, headCard);

        // Set header data from data object
        TextView tvType = headCard.findViewById(R.id.title);
        TextView tvModel = headCard.findViewById(R.id.tv_model_year);
        TextView tvHint = headCard.findViewById(R.id.tv_hint);
        TextView tvPlateNumber = headCard.findViewById(R.id.tv_plate_number);

        tvType.setText(carCard.getHeadTitle());
        tvModel.setText(carCard.getCarModel() + "\t - \t" +carCard.getYear());
        tvPlateNumber.setText(carCard.getPlateNumber());
        tvHint.setText(carCard.getPersonMessage());
    }


    @SuppressLint("RestrictedApi")
    private void fabVisibilty() {
        if (isViewPagerCollapse) {
            fabAddCar.hide();
        } else {
            fabAddCar.show();
        }
    }

    @Override
    public void onBackPressed() {
        if (ecPagerElement.collapse()) {
            isViewPagerCollapse = !isViewPagerCollapse;
            fabAddCar.show();
            return;
        }
        super.onBackPressed();
    }

//    public static float dpFromPx(final Context context, final float px) {
//        return px / context.getResources().getDisplayMetrics().density;
//    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    @OnClick(R.id.fab_add_car)
    public void onViewClicked() {
        startActivity(new Intent(this, CarActivity.class));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (isGPSEnable(context)) {
                   reserveArrayAdapter.getLocation();
                } else {
                    showDialogEnableLocation(context);
                }
                Toast.makeText(this, "Permission was granted", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(this, "requestCode != 0", Toast.LENGTH_SHORT).show();

            super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }


    }

    class CustomEcPagerViewAdapter extends ECPagerViewAdapter {

        public CustomEcPagerViewAdapter(Context applicationContext, List<ECCardData> dataset) {
            super(applicationContext, dataset);
        }

        @Override
        public void instantiateCard(LayoutInflater inflaterService, ViewGroup headCard, ListView list, ECCardData data) {
            final CarCard carCard = (CarCard) data;
            reserveArrayAdapter = new ReserveArrayAdapter(context,
                    ((CarCard) data).getIdCarType(), ((CarCard) data).getIdMyCar());

            list.setAdapter(new ReserveArrayAdapter(context, ((CarCard) data).getIdCarType(), ((CarCard) data).getIdMyCar()));
            list.setDivider(getResources().getDrawable(R.drawable.list_divider));
            list.setDividerHeight((int) pxFromDp(getApplicationContext(), 0.5f));
            list.setSelector(R.color.transparent);
            list.setBackgroundColor(Color.WHITE);
            list.setCacheColorHint(Color.TRANSPARENT);

            configureHeadCard(inflaterService, headCard, carCard);

            headCard.setOnClickListener(v -> {
                ecPagerElement.toggle();
                isViewPagerCollapse = !isViewPagerCollapse;
                fabVisibilty();
            });
        }
    }
}
