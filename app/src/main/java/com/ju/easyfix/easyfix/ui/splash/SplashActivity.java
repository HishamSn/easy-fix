package com.ju.easyfix.easyfix.ui.splash;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.data.prefs.PrefsUtils;
import com.ju.easyfix.easyfix.ui.admin.main.AdminActivity;
import com.ju.easyfix.easyfix.ui.auth.LoginActivity;
import com.ju.easyfix.easyfix.ui.user.main.MainActivity;
import com.ju.easyfix.easyfix.util.SessionUtils;
import com.squareup.picasso.Picasso;

public class SplashActivity extends AppCompatActivity {


    private ImageView ivSplash;
    private AnimationListener.Stop stopAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ivSplash = findViewById(R.id.iv_splash);

        Picasso.with(this).load(R.drawable.logo).into(ivSplash);
        openActivityAnimationStop();

        initAnimation();


        YoYo.with(Techniques.RotateInDownRight)
                .duration(3000)
                .playOn(findViewById(R.id.tv_app_name));
    }


    private void openActivityAnimationStop() {
        stopAnimation = () -> {
            if (SessionUtils.getInstance().isLogin()) {
                if (PrefsUtils.getInstance().isAdmin()) {
                    goToActivity(AdminActivity.class);
                } else {
                    goToActivity(MainActivity.class);
                }
            } else {
                goToActivity(LoginActivity.class);
            }
            finish();
        };
    }

    private void goToActivity(Class activity) {
        startActivity(new Intent
                (this, activity));
    }


    private void initAnimation() {
        ViewAnimator
                .animate(ivSplash)
                .scale(0, 1)
                .onStop(stopAnimation)
                .start();
    }
}
