package com.ju.easyfix.easyfix.model;


import com.ju.easyfix.easyfix.explib.ECCardData;

import java.util.List;

public class CarCard implements ECCardData<ReserveModel> {

    private String idCarType;

    private String headTitle;
    private Integer headBackgroundResource;
    private Integer mainBackgroundResource;
    private String urlImage;

    private String carType;
    private String carModel;
    private String Year;
    private String plateNumber;
    private String idMyCar;


    private List<ReserveModel> listItems;

    public CarCard() {

    }


    @Override
    public String getUrlImage() {
        return urlImage;
    }


    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getHeadTitle() {
        return headTitle;
    }

    public void setHeadTitle(String headTitle) {
        this.headTitle = headTitle;
    }

    public Integer getHeadBackgroundResource() {
        return headBackgroundResource;
    }

    public void setHeadBackgroundResource(Integer headBackgroundResource) {
        this.headBackgroundResource = headBackgroundResource;
    }

    public Integer getMainBackgroundResource() {
        return mainBackgroundResource;
    }

    public void setMainBackgroundResource(Integer mainBackgroundResource) {
        this.mainBackgroundResource = mainBackgroundResource;
    }


    public String getPersonMessage() {
        return "Reserve Now";
    }


    @Override
    public List<ReserveModel> getListItems() {
        return listItems;
    }

    public void setListItems(List<ReserveModel> listItems) {
        this.listItems = listItems;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getIdCarType() {
        return idCarType;
    }

    public void setIdCarType(String idCarType) {
        this.idCarType = idCarType;
    }

    public String getIdMyCar() {
        return idMyCar;
    }

    public void setIdMyCar(String idMyCar) {
        this.idMyCar = idMyCar;
    }
}
