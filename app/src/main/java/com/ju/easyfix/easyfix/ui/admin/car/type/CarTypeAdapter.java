package com.ju.easyfix.easyfix.ui.admin.car.type;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.admin.CarType;

import java.util.List;

import static com.ju.easyfix.easyfix.constant.FirebaseConstant.COMPANY_TABLE;

public class CarTypeAdapter extends RecyclerView.Adapter<CarTypeAdapter.ViewHolder> {
    private static final int ROW_CAR_TYPE = R.layout.row_car;
    private List<CarType> carTypeList;
    private Context context;
    private AlertDialog.Builder builder;
    private DatabaseReference mDatabaseReference;

    public CarTypeAdapter(List<CarType> myOrders) {
        this.carTypeList = myOrders;
    }

    @NonNull
    @Override
    public CarTypeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @Override
    public void onBindViewHolder(@NonNull CarTypeAdapter.ViewHolder holder, int position) {
        holder.tvName.setText(carTypeList.get(position).getName());

        holder.itemView.setOnLongClickListener(v -> {
            dialogInput(position);

            return false;
        });
    }


    private void dialogInput(int position) {
        builder = new AlertDialog.Builder(context);
        builder.setTitle("Car Type Name");
        final EditText etCarTypeName = new EditText(context);
        etCarTypeName.setText(carTypeList.get(position).getName());
        builder.setView(etCarTypeName);

        builder.setPositiveButton("OK", (dialog, which) -> {
            mDatabaseReference = FirebaseDatabase.getInstance().getReference().child(COMPANY_TABLE).child(carTypeList.get(position).getId());

            mDatabaseReference.setValue(new CarType(etCarTypeName.getText().toString(), carTypeList.get(position).getCarModels()));


        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();
    }

    @Override
    public int getItemCount() {
        return carTypeList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return ROW_CAR_TYPE;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;

        public ViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tv_name);

        }
    }


}
