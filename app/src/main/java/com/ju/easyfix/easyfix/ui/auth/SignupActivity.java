package com.ju.easyfix.easyfix.ui.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.user.User;
import com.ju.easyfix.easyfix.ui.base.BaseActivity;
import com.ju.easyfix.easyfix.ui.user.main.MainActivity;
import com.ju.easyfix.easyfix.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupActivity extends BaseActivity {
    @BindView(R.id.et_name)
    AppCompatEditText etName;
    @BindView(R.id.et_phone)
    AppCompatEditText etPhone;
    @BindView(R.id.et_password)
    AppCompatEditText etPassword;
    @BindView(R.id.et_email)
    AppCompatEditText etEmail;
    private FirebaseAuth mAuth;
    private ProgressDialog progress;

    private DatabaseReference mDatabaseReference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        init();

    }

    private void init() {
        progress = Util.progressUtil(this);
        mAuth = FirebaseAuth.getInstance();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("Users");
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }

    @OnClick(R.id.btn_sign_up)
    public void onViewClicked() {
        progress.show();
        mAuth.createUserWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                .addOnCompleteListener(this, task -> {
                    progress.dismiss();
                    if (task.isSuccessful()) {


                        Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                        User user = new User();
                        user.setId(mAuth.getCurrentUser().getUid());
                        user.setEmail(etEmail.getText().toString());
                        user.setName(etName.getText().toString());
                        user.setPhone(etPhone.getText().toString());
                        mDatabaseReference.child(mAuth.getCurrentUser().getUid()).setValue(user);
                        startActivity(new Intent(this, MainActivity.class));
                        finish();


                    } else {
                        Toast.makeText(this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
    }
}

