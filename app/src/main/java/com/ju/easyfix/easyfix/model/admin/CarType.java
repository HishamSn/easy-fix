package com.ju.easyfix.easyfix.model.admin;


import com.ju.easyfix.easyfix.model.BusyDate;
import com.ju.easyfix.easyfix.model.Reservation;
import com.ju.easyfix.easyfix.model.ReserveHelp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CarType implements Serializable {

    private String id;
    private String name;

    private List<CarModel> carModels = new ArrayList<>();
    private List<BusyDate> busyDate = new ArrayList<>();
    private List<Reservation> reservation = new ArrayList<>();
    private List<ReserveHelp> reserveHelp = new ArrayList<>();

    public CarType() {
    }

    public CarType(String name, List<CarModel> carModels) {
        this.name = name;
        this.carModels = carModels;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CarModel> getCarModels() {
        return carModels;
    }

    public void setCarModels(List<CarModel> carModels) {
        this.carModels = carModels;
    }


    public List<BusyDate> getBusyDate() {
        return busyDate;
    }

    public void setBusyDate(List<BusyDate> busyDate) {
        this.busyDate = busyDate;
    }

    public List<Reservation> getReservation() {
        return reservation;
    }

    public void setReservation(List<Reservation> reservation) {
        this.reservation = reservation;
    }

    public List<ReserveHelp> getReserveHelp() {
        return reserveHelp;
    }

    public void setReserveHelp(List<ReserveHelp> reserveHelp) {
        this.reserveHelp = reserveHelp;
    }
}
