package com.ju.easyfix.easyfix.ui.admin.setting;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.ModelRowImage;
import com.ju.easyfix.easyfix.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarsSettingActivity extends BaseActivity {

    @BindView(R.id.rv_main)
    RecyclerView rvMain;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);

        init();
        setUpRecyclerView(getMainList());
    }


    private void init() {
        rvMain = findViewById(R.id.rv_main);
    }

    private void setUpRecyclerView(List<ModelRowImage> mainList) {
        rvMain.setLayoutManager(
                new LinearLayoutManager(this));

        rvMain.setAdapter(new CarsSettingAdapter(mainList));
    }

    @NonNull
    private List<ModelRowImage> getMainList() {
        List<ModelRowImage> mainList = new ArrayList<>();
        mainList.add(new ModelRowImage("Car Type", R.drawable.bg_cars));
        mainList.add(new ModelRowImage("Car Model", R.drawable.bg_car_model));
//        mainList.add(new ModelRowImage("Test", R.drawable.car_workshop));

        return mainList;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);

    }

}
