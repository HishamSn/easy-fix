package com.ju.easyfix.easyfix.ui.admin.car.model;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.admin.CarModel;
import com.ju.easyfix.easyfix.model.admin.CarType;
import com.ju.easyfix.easyfix.ui.admin.car.model.adapter.SpCarTypeAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ju.easyfix.easyfix.constant.FirebaseConstant.COMPANY_TABLE;

public class AddCarModelActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {


    @BindView(R.id.sp_car_type)
    AppCompatSpinner spCarType;
    @BindView(R.id.rv_car_model)
    RecyclerView rvCarModel;
    @BindView(R.id.fab_add_car_model)
    FloatingActionButton fab;
    private List<CarType> carTypeList = new ArrayList<>();
    private List<CarModel> carModelList = new ArrayList<>();
    private DatabaseReference mDatabaseReference;
    private Context context = this;
    private String carTypeName;
    private AlertDialog.Builder builder;
    private String idCarType;
    private int idCarSelected;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_car_model_activity);
        ButterKnife.bind(this);
        initSpinner();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        initRecyclerView(layoutManager);

        getCarType();
    }

    private void initSpinner() {
        spCarType.setOnItemSelectedListener(this);

    }

    private void initRecyclerView(LinearLayoutManager layoutManager) {
        rvCarModel.setLayoutManager(layoutManager);
        rvCarModel.addItemDecoration(new DividerItemDecoration(rvCarModel.getContext(),
                layoutManager.getOrientation()));
//        rvCarModel.setAdapter(new CarModelAdapter(carTypeList.get(0)));
    }


    private void getCarType() {

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child(COMPANY_TABLE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                carTypeList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    CarType carType = child.getValue(CarType.class);
                    carType.setId(child.getKey());
                    carTypeList.add(carType);
                }
                spCarType.setAdapter(new SpCarTypeAdapter(context, carTypeList));
//                rvCarModel.getAdapter().notifyDataSetChanged();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void dialogInput() {

        builder = new AlertDialog.Builder(this);
        builder.setTitle("Car Model Add");

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_car_model, null);
        builder.setView(dialogView);

        EditText etCarTypeName = dialogView.findViewById(R.id.et_car_model_name);
//        EditText etCarYear = dialogView.findViewById(R.id.et_car_model_year);

        Button btnOk = dialogView.findViewById(R.id.btn_ok);
        Button btnCancel = dialogView.findViewById(R.id.btn_cancel);
        AlertDialog dialog = builder.create();


        btnOk.setOnClickListener(v -> {
            CarModel carModel = new CarModel();
            carModel.setId(rvCarModel.getAdapter().getItemCount() + "");
            carModel.setName(etCarTypeName.getText().toString());
//            carModel.setYear(etCarYear.getText().toString());
            carModel.setImage("http://");
            carModel.setImageBackground("http://");


            carTypeList.get(idCarSelected).getCarModels().add(carModel);
            mDatabaseReference = FirebaseDatabase.getInstance().getReference().child(COMPANY_TABLE);

            mDatabaseReference.setValue(carTypeList);
            dialog.dismiss();
        });
        btnCancel.setOnClickListener(v -> {
            dialog.dismiss();
        });
        dialog.show();
    }

    @OnClick(R.id.fab_add_car_model)
    public void onViewClicked() {
        dialogInput();

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.sp_car_type:
                carModelList.clear();
                if (carTypeList.get(position).getCarModels() != null) {
                    carModelList.addAll(carTypeList.get(position).getCarModels());
                }
                rvCarModel.setAdapter(new CarModelAdapter(carTypeList.get(position)));

                idCarSelected = position;
                idCarType = carTypeList.get(position).getId();

//                rvCarModel.getAdapter().notifyDataSetChanged();

                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
