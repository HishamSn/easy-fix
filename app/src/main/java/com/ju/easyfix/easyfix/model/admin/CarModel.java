package com.ju.easyfix.easyfix.model.admin;

import java.io.Serializable;

public class CarModel implements Serializable {

    private String id;
    private String name;

//    private String year;
    private String image;
    private String imageBackground;


    public CarModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

//    public String getYear() {
//        if (year == null) {
//            return "";
//        }
//        return year;
//    }
//
//    public void setYear(String year) {
//        this.year = year;
//    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageBackground() {
        return imageBackground;
    }

    public void setImageBackground(String imageBackground) {
        this.imageBackground = imageBackground;
    }
}
