package com.ju.easyfix.easyfix.ui.admin.car.model.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.admin.CarModel;

import java.util.List;


public class SpCarModelAdapter extends ArrayAdapter<CarModel> {

    public SpCarModelAdapter(Context context, List<CarModel> carTypeList) {
        super(context, R.layout.item_spinner_row, carTypeList);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return firstView(position, convertView, parent);
    }

    private View firstView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_spinner_header, parent, false
            );
        }

        TextView textViewName = view.findViewById(R.id.tv_item_spinner);

        CarModel carModel = getItem(position);
        if (carModel != null) {
            textViewName.setText(carModel.getName());
        }

        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_spinner_row, parent, false
            );
        }

        TextView textViewName = view.findViewById(R.id.tv_item_spinner);


        CarModel carModel = getItem(position);

        if (carModel != null) {
            textViewName.setText(carModel.getName());
        }

        return view;
    }


}