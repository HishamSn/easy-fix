package com.ju.easyfix.easyfix.model.admin;

public class CarYear {

    private String year;
    private String image;
    private String background;

    public CarYear() {
    }

    public CarYear(String year, String image, String background) {
        this.year = year;
        this.image = image;
        this.background = background;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }
}
