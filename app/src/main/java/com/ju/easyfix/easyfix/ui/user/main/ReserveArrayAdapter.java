package com.ju.easyfix.easyfix.ui.user.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.explib.ECCardContentListItemAdapter;
import com.ju.easyfix.easyfix.model.ReserveHelp;
import com.ju.easyfix.easyfix.model.ReserveModel;
import com.ju.easyfix.easyfix.model.user.MyCars;
import com.ju.easyfix.easyfix.model.user.User;
import com.ju.easyfix.easyfix.ui.user.history.HistoryLocationActivity;
import com.ju.easyfix.easyfix.util.SessionUtils;
import com.ju.easyfix.easyfix.util.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.nlopez.smartlocation.SmartLocation;

import static com.ju.easyfix.easyfix.constant.FirebaseConstant.COMPANY_TABLE;
import static com.ju.easyfix.easyfix.constant.FirebaseConstant.MY_CAR_TABLE;
import static com.ju.easyfix.easyfix.constant.FirebaseConstant.RESERVE_HELP_TABLE;
import static com.ju.easyfix.easyfix.constant.FirebaseConstant.USERS_TABLE;
import static com.ju.easyfix.easyfix.util.LocationUtil.isGPSEnable;
import static com.ju.easyfix.easyfix.util.LocationUtil.showDialogEnableLocation;

@SuppressLint({"SetTextI18n", "InflateParams"})
public class ReserveArrayAdapter extends ECCardContentListItemAdapter<ReserveModel> {
    private FragmentActivity context;
    private String idCarType;
    private String idMyCar;
    private String latitude;
    private String longitude;
    private String dateSelected;
    private Calendar calendar;
    private int currentDay, currentMonth, cuurentYear;
    private Date date;
    private SimpleDateFormat formatter;
    private MyCars myCar;

    private List<ReserveHelp> reserveHelpList;
    private DatabaseReference databaseReference;
    private ProgressDialog progress;

    public ReserveArrayAdapter(@NonNull Context context, String idCarType, String idMyCar) {
        super(context, R.layout.layout_reserve, new ArrayList<>());
        this.context = (FragmentActivity) context;
        this.idCarType = idCarType;
        this.idMyCar = idMyCar;
        init();
        defaultSelected();
        getMyCar();
        getReserveHelp();
        SessionUtils.getInstance();
    }

    private void init() {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        reserveHelpList = new ArrayList<>();
        progress = Util.progressUtil(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());

            rowView = inflater.inflate(R.layout.layout_reserve, null);
            // configure view holder
            viewHolder = new ViewHolder();
            viewHolder.btnReserve = rowView.findViewById(R.id.cv_reserve);
            viewHolder.btnHelp = rowView.findViewById(R.id.cv_help);

            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        viewHolder.btnReserve.setOnClickListener(v -> {
            BottomSheetDialogFragment dialog = new ReserveBottomSheetDialog();
            Bundle data = new Bundle();
            data.putString("CAR_TYPE_ID", idCarType);
            data.putString("MY_CAR_ID", idMyCar);
            dialog.setArguments(data);
            dialog.show(context.getSupportFragmentManager(), "Dialog");
        });

        viewHolder.btnHelp.setOnClickListener(v -> {

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                Toast.makeText(context, "Location Permission is already available",
//                        Toast.LENGTH_SHORT).show();
                if (isGPSEnable(context)) {
                    getLocation();
                } else {
                    showDialogEnableLocation(context);
                }

            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Toast.makeText(context, "Location Permission is needed to show the location preview",
                            Toast.LENGTH_SHORT).show();

                }
                ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            }

        });

        return rowView;
    }

    private void getMyCar() {
        DatabaseReference busyDatabaseDate = databaseReference.child(USERS_TABLE)
                .child(SessionUtils.getInstance().mAuth().getUid()).child(MY_CAR_TABLE);
        busyDatabaseDate.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    MyCars myCarTemp = child.getValue(MyCars.class);

                    if (myCarTemp.getId().equals(idMyCar)) {
                        myCar = myCarTemp;
                        return;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getLocation() {
        SmartLocation.with(context).location()
                .continuous()
                .start(location -> {
                    progress.show();
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm aa");
                    String datetime = dateformat.format(c.getTime());


                    latitude = location.getLatitude() + "";
                    longitude = location.getLongitude() + "";

                    Log.d("location", latitude + "," + longitude);
                    DatabaseReference reserveDatabaseDate = databaseReference.child(COMPANY_TABLE)
                            .child(idCarType).child(RESERVE_HELP_TABLE);
                    ReserveHelp reserveHelp = new ReserveHelp();
                    reserveHelp.setId(reserveHelpList.size() + "");
                    reserveHelp.setIdCustomerName(SessionUtils.getInstance().mAuth().getUid());
                    reserveHelp.setIdCustomerCar(idMyCar);
                    reserveHelp.setIdCarType(myCar.getIdCarType());
                    reserveHelp.setIdCarModel(myCar.getIdCarModel());
                    reserveHelp.setCarType(myCar.getType());
                    reserveHelp.setCarModel(myCar.getModel());
                    reserveHelp.setCarYear(myCar.getYear());
                    reserveHelp.setPlateNumber(myCar.getPlateNumber());
                    reserveHelp.setImage(myCar.getImage());
                    reserveHelp.setDate(dateSelected);
                    reserveHelp.setTime(datetime);
                    SessionUtils.getInstance().getDatabaseReference().addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progress.dismiss();
                            User user = dataSnapshot.getValue(User.class);
                            reserveHelp.setNameCustomer(user.getName());
                            reserveHelp.setPhone(user.getPhone());


                            reserveHelp.setLatitude(latitude);
                            reserveHelp.setLongitude(longitude);
                            reserveHelpList.add(reserveHelp);

                            reserveDatabaseDate.setValue(reserveHelpList);
                            Toast.makeText(context, "Reserved", Toast.LENGTH_SHORT).show();

                            context.startActivity(new Intent(context, HistoryLocationActivity.class));
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progress.dismiss();
                        }
                    });
                });
    }

    private void getReserveHelp() {
        DatabaseReference busyDatabaseDate = databaseReference.child(COMPANY_TABLE)
                .child(idCarType).child(RESERVE_HELP_TABLE);
        busyDatabaseDate.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                reserveHelpList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    ReserveHelp reserveHelp = child.getValue(ReserveHelp.class);
                    reserveHelpList.add(reserveHelp);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void defaultSelected() {
        calendar = Calendar.getInstance();
        formatter = new SimpleDateFormat("dd");
        date = new Date();

        currentDay = Integer.parseInt(formatter.format(date));
        currentMonth = Calendar.getInstance().get(Calendar.MONTH);
        cuurentYear = Calendar.getInstance().get(Calendar.YEAR);

        calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, cuurentYear);
        calendar.set(Calendar.MONTH, currentMonth);
        dateSelected = cuurentYear + "-" + (currentMonth + 1) + "-" + (currentDay);

        calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, cuurentYear);
        calendar.set(Calendar.MONTH, currentMonth);
        dateSelected = cuurentYear + "-" + (currentMonth + 1) + "-" + (currentDay);
        Log.e("Default Selected : ", dateSelected);
    }


    static class ViewHolder {
        CardView btnReserve;
        CardView btnHelp;
    }


}
