package com.ju.easyfix.easyfix.ui.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.ui.about.AboutActivity;
import com.ju.easyfix.easyfix.ui.about.RateActivity;
import com.ju.easyfix.easyfix.ui.auth.LoginActivity;
import com.ju.easyfix.easyfix.ui.user.history.HistoryActivity;
import com.ju.easyfix.easyfix.ui.user.history.HistoryLocationActivity;
import com.ju.easyfix.easyfix.util.SessionUtils;


@SuppressLint("Registered")
public abstract class BaseActivity extends AppCompatActivity {

    View viewHeaderNav;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private ActionBarDrawerToggle toggleBar;
    private NavigationView navigationView;
    private Context context = this;
    private Button btnLogin;
    private Button btnSetting;
    private Intent intent;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    public void setNavigation(NavigationView navigationView, Toolbar toolbar,
                              DrawerLayout drawerLayout) {
        init(navigationView, toolbar, drawerLayout);

        toggleBar = new ActionBarDrawerToggle(this, drawerLayout, toolbar
                , R.string.my_addresses, R.string.my_addresses);
        drawerLayout.addDrawerListener(toggleBar);
        toggleBar.syncState();
        navigationView.getMenu().getItem(0).setChecked(true);
        setNavigationItemSelectedListener(navigationView);


        setSupportActionBar(toolbar);
        toolbar.setTitle("Our Service");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        addHeader(navigationView);


    }

    private void addHeader(NavigationView navigationView) {
        viewHeaderNav = navigationView.inflateHeaderView(R.layout.nav_header);
    }


    private void init(NavigationView navigationView, Toolbar toolbar, DrawerLayout drawerLayout) {
        this.navigationView = navigationView;
        this.drawerLayout = drawerLayout;
        this.toolbar = toolbar;

    }


    private void setNavigationItemSelectedListener(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(item -> {
            selectItemDrawer(item);
            return true;
        });
    }


    private void selectItemDrawer(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_history:

                startActivity(new Intent(this, HistoryActivity.class));

                break;
            case R.id.menu_history_location:
                startActivity(new Intent(this, HistoryLocationActivity.class));

                break;

            case R.id.menu_about:
                startActivity(new Intent(this, AboutActivity.class));

                break;

            case R.id.menu_rate_us:
                startActivity(new Intent(this, RateActivity.class));

                break;

        }
        drawerLayout.closeDrawers();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_logout) {
            SessionUtils.getInstance().logout();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return true;
        }
        if (toggleBar.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (navigationView != null) {
        }
    }

}
