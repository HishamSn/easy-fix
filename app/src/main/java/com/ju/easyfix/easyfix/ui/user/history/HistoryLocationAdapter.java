package com.ju.easyfix.easyfix.ui.user.history;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.ReserveHelp;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HistoryLocationAdapter extends RecyclerView.Adapter<HistoryLocationAdapter.ViewHolder> {
    private static final int ROW_HISTORY_LOCATION = R.layout.row_history_location;
    private List<ReserveHelp> reserveHelpList;
    private Context context;

    public HistoryLocationAdapter(List<ReserveHelp> reserveHelpList) {
        this.reserveHelpList = reserveHelpList;
    }

    @NonNull
    @Override
    public HistoryLocationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryLocationAdapter.ViewHolder holder, int position) {
        ReserveHelp reserveHelp = reserveHelpList.get(position);
        holder.tvId.setText("#\t" + reserveHelp.getIdCarType() + " - " + reserveHelp.getId());
        holder.tvCarType.setText(reserveHelp.getCarType());
        holder.tvCarModel.setText(reserveHelp.getCarModel());
        holder.tvCarYear.setText(reserveHelp.getCarYear());
        holder.tvPlateNumber.setText(reserveHelp.getPlateNumber());
        holder.tvDate.setText(reserveHelp.getDate());
        holder.tvTime.setText(reserveHelp.getTime());
        Picasso.with(context).load(reserveHelp.getImage()).into(holder.ivCarImage);

    }


    @Override
    public int getItemCount() {
        return reserveHelpList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return ROW_HISTORY_LOCATION;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvId;
        private TextView tvDate;
        private TextView tvTime;
        private TextView tvCarType;
        private TextView tvCarModel;
        private TextView tvCarYear;
        private TextView tvPlateNumber;
        private ImageView ivCarImage;


        public ViewHolder(View view) {
            super(view);
            tvId = view.findViewById(R.id.tv_reserve_id);
            tvDate = view.findViewById(R.id.tv_date);
            tvTime = view.findViewById(R.id.tv_time);
            tvCarType = view.findViewById(R.id.tv_car_type);
            tvCarModel = view.findViewById(R.id.tv_car_model);
            tvCarYear = view.findViewById(R.id.tv_car_year);
            tvPlateNumber = view.findViewById(R.id.tv_plate_number);
            ivCarImage = view.findViewById(R.id.iv_car);

        }
    }


}
