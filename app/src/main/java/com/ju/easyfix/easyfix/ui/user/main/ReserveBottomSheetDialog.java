package com.ju.easyfix.easyfix.ui.user.main;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.listener.IReserveListener;
import com.ju.easyfix.easyfix.model.BusyDate;
import com.ju.easyfix.easyfix.model.Reservation;
import com.ju.easyfix.easyfix.model.user.MyCars;
import com.ju.easyfix.easyfix.model.user.User;
import com.ju.easyfix.easyfix.ui.user.history.HistoryActivity;
import com.ju.easyfix.easyfix.util.SessionUtils;
import com.ju.easyfix.easyfix.util.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.ju.easyfix.easyfix.constant.FirebaseConstant.BUSY_DATE_TABLE;
import static com.ju.easyfix.easyfix.constant.FirebaseConstant.COMPANY_TABLE;
import static com.ju.easyfix.easyfix.constant.FirebaseConstant.MY_CAR_TABLE;
import static com.ju.easyfix.easyfix.constant.FirebaseConstant.RESERVATION_TABLE;
import static com.ju.easyfix.easyfix.constant.FirebaseConstant.USERS_TABLE;

public class ReserveBottomSheetDialog extends BottomSheetDialogFragment implements IReserveListener {

    Unbinder unbinder;
    @BindView(R.id.rv_reservation_time)
    RecyclerView rvReservationTime;
    @BindView(R.id.btn_choose_date)
    AppCompatButton btnChooseDate;
    @BindView(R.id.radio_group)
    RadioGroup rgDays;
    @BindView(R.id.rb_today)
    AppCompatRadioButton rbToday;
    @BindView(R.id.rb_tomorrow)
    AppCompatRadioButton rbTomorrow;

    IReserveListener iReserveListener = this;
    @BindView(R.id.et_reservation_reason)
    AppCompatEditText etReservationReason;

    private Activity activity;
    private String idCarType;
    private String idMyCar;
    private SimpleDateFormat formatter;

    private String dateSelected;
    private int timeIdSelected;
    private int branchIdSelected;
    private int numberOfPeopleSelected;
    private int currentDay, currentMonth, cuurentYear;
    private int daysInMonth;
    private Date date;
    private Calendar calendar;

    private DatePickerDialog datePickerDialog;
    private List<String> timeAvailableArray;
    private List<BusyDate> busyDateList;
    private List<Reservation> reservationList;
    private MyCars myCar;
    private DatabaseReference databaseReference;
    private String timeSelected;
    private ProgressDialog progress;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_reserve_table, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDateTimePicker();
        init();
        getBundle();
        setAdapter();
        setUpRadioButtonChecked();
        defaultSelected();
        checkTimeAvailable();
        getMyCar();
        getReservation();
//        getReservationTimeChosen(dateSelected);
    }


    public void getBundle() {
        idCarType = getArguments().getString("CAR_TYPE_ID");
        idMyCar = getArguments().getString("MY_CAR_ID");
    }

    private void init() {
        progress = Util.progressUtil(getContext());

        timeAvailableArray = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.time_available)));
        databaseReference = FirebaseDatabase.getInstance().getReference();
        busyDateList = new ArrayList<>();
        reservationList = new ArrayList<>();
        calendar = Calendar.getInstance();
        activity = getActivity();
        formatter = new SimpleDateFormat("dd");
        date = new Date();


        currentDay = Integer.parseInt(formatter.format(date));
        currentMonth = Calendar.getInstance().get(Calendar.MONTH);
        cuurentYear = Calendar.getInstance().get(Calendar.YEAR);

        calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, cuurentYear);
        calendar.set(Calendar.MONTH, currentMonth);
        daysInMonth = calendar.getActualMaximum(Calendar.DATE);
        dateSelected = cuurentYear + "-" + (currentMonth + 1) + "-" + (currentDay);

        Log.e("Today : ", dateSelected);

    }

    private void initDateTimePicker() {
        datePickerDialog = new DatePickerDialog(getContext(),
                (view1, yearDate, monthDate, dayDate) -> {

                    dateSelected = yearDate + "-" + (monthDate) + "-" + dayDate;

                    if (cuurentYear == yearDate && currentMonth + 1 == monthDate && currentDay == dayDate) {
                        btnChooseDate.setText(R.string.choose_date);
                        rbToday.setChecked(true);
                    } else if (cuurentYear == yearDate &&
                            currentMonth + 1 == monthDate && currentDay + 1 == dayDate) {
                        btnChooseDate.setText(R.string.choose_date);
                        rbTomorrow.setChecked(true);

                    } else {
                        rbToday.setChecked(false);
                        rbTomorrow.setChecked(false);
                        btnChooseDate.setText(dateSelected);
                    }

                    dateSelected = yearDate + "-" + (monthDate + 1) + "-" + dayDate;
                    btnChooseDate.setText(dateSelected);
                }, cuurentYear, currentMonth, currentDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
    }

    private void setUpRadioButtonChecked() {
        rgDays.setOnCheckedChangeListener((group, checkedId) -> {

            switch (checkedId) {
                case R.id.rb_today:
                    timeAvailableArray = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.time_available)));

                    calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, cuurentYear);
                    calendar.set(Calendar.MONTH, currentMonth);
                    daysInMonth = calendar.getActualMaximum(Calendar.DATE);
                    dateSelected = cuurentYear + "-" + (currentMonth + 1) + "-" + (currentDay);
                    Log.e("Today : ", dateSelected);

                    break;
                case R.id.rb_tomorrow:
                    timeAvailableArray = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.time_available)));

                    calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, cuurentYear);
                    calendar.set(Calendar.MONTH, currentMonth);
                    daysInMonth = calendar.getActualMaximum(Calendar.DATE);
                    if (currentDay <= daysInMonth) {
                        dateSelected = cuurentYear + "-" + (currentMonth + 1) + "-" + (currentDay + 1);
                    } else if (currentMonth >= 12) {
                        currentMonth = 1;
                        dateSelected = cuurentYear + "-" + (currentMonth) + "-" + 1;
                    } else {
                        dateSelected = cuurentYear + "-" + (currentMonth + 1) + "-" + (currentDay + 1);
                    }
                    dateSelected = cuurentYear + "-" + (currentMonth + 1) + "-" + (currentDay + 1);
                    Log.e("Tomorrow : ", dateSelected);

                    break;

            }
            btnChooseDate.setText(dateSelected);
            checkTimeAvailable();

        });
    }

    private void checkTimeAvailable() {
        progress.show();
        DatabaseReference busyDatabaseDate = databaseReference.child(COMPANY_TABLE).child(idCarType).child(BUSY_DATE_TABLE);
        busyDatabaseDate.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progress.dismiss();
                busyDateList.clear();

                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    BusyDate busyDate = child.getValue(BusyDate.class);
                    busyDate.setId(child.getKey());
                    busyDateList.add(busyDate);

                    if (dateSelected.contains(busyDate.getDate())
                            && timeAvailableArray.contains(busyDate.getTime())) {
                        timeAvailableArray.remove(busyDate.getTime());
                    }
                }
                if (rvReservationTime != null) {
                    rvReservationTime.setAdapter(new ReservationTimeAdapter(timeAvailableArray, iReserveListener));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progress.dismiss();
                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getMyCar() {
        progress.show();
        DatabaseReference busyDatabaseDate = databaseReference.child(USERS_TABLE)
                .child(SessionUtils.getInstance().mAuth().getUid()).child(MY_CAR_TABLE);
        busyDatabaseDate.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progress.dismiss();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    MyCars myCarTemp = child.getValue(MyCars.class);

                    if (myCarTemp.getId().equals(idMyCar)) {
                        myCar = myCarTemp;
                        return;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progress.dismiss();
                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getReservation() {
        progress.show();
        DatabaseReference busyDatabaseDate = databaseReference.child(COMPANY_TABLE)
                .child(idCarType).child(RESERVATION_TABLE);
        busyDatabaseDate.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progress.dismiss();
                reservationList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    Reservation reservation = child.getValue(Reservation.class);
                    reservationList.add(reservation);
                }
                reservationList.size();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progress.dismiss();
                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void defaultSelected() {
        numberOfPeopleSelected = 1;
        calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, cuurentYear);
        calendar.set(Calendar.MONTH, currentMonth);
        daysInMonth = calendar.getActualMaximum(Calendar.DATE);
        dateSelected = cuurentYear + "-" + (currentMonth + 1) + "-" + (currentDay);
        Log.e("Default Selected : ", dateSelected);
    }



    private void setAdapter() {

        rvReservationTime.setAdapter(new ReservationTimeAdapter(timeAvailableArray, this));
    }

    @OnClick({R.id.btn_close_reservation, R.id.btn_reserve_table, R.id.btn_choose_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_close_reservation:
                dismiss();
                break;
            case R.id.btn_reserve_table:
                Log.e("Today Date : ", dateSelected);

                DatabaseReference busyDatabaseDate = databaseReference.child(COMPANY_TABLE).child(idCarType).child(BUSY_DATE_TABLE);
                DatabaseReference reserveDatabaseDate = databaseReference.child(COMPANY_TABLE).child(idCarType).child(RESERVATION_TABLE);

                BusyDate busyDate = new BusyDate();
                busyDate.setId(busyDateList.size() + "");
                busyDate.setDate(dateSelected);
                busyDate.setTime(timeSelected);
                busyDateList.add(busyDate);

                Reservation reservation = new Reservation();
                reservation.setId(reservationList.size() + "");
                reservation.setIdCustomerName(SessionUtils.getInstance().mAuth().getUid());
                reservation.setIdCustomerCar(idMyCar);
                reservation.setIdCarType(myCar.getIdCarType());
                reservation.setIdCarModel(myCar.getIdCarModel());
                reservation.setCarType(myCar.getType());
                reservation.setCarModel(myCar.getModel());
                reservation.setCarYear(myCar.getYear());
                reservation.setPlateNumber(myCar.getPlateNumber());
                reservation.setImage(myCar.getImage());
                reservation.setDate(dateSelected);
                reservation.setTime(timeSelected);
                reservation.setDescription(etReservationReason.getText().toString());

                SessionUtils.getInstance().getDatabaseReference().addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        reservation.setNameCustomer(user.getName());
                        reservation.setPhone(user.getPhone());
                        reservationList.add(reservation);
                        busyDatabaseDate.setValue(busyDateList);
                        reserveDatabaseDate.setValue(reservationList);
                        startActivity(new Intent(getContext(), HistoryActivity.class));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progress.dismiss();
                    }
                });




                break;
            case R.id.btn_choose_date:
                datePickerDialog.show();
                break;
        }
    }

    @Override
    public void getReservationTimeChosen(String time) {
        timeSelected = time;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
