package com.ju.easyfix.easyfix.ui.admin.help;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.ReserveHelp;
import com.ju.easyfix.easyfix.util.Util;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ju.easyfix.easyfix.constant.FirebaseConstant.RESERVE_HELP_TABLE;

public class HelpDetailsActivity extends FragmentActivity implements OnMapReadyCallback {

    @BindView(R.id.tv_customer_name)
    AppCompatTextView tvCustomerName;
    @BindView(R.id.tv_phone)
    AppCompatTextView tvPhone;
    @BindView(R.id.tv_time)
    AppCompatTextView tvTime;
    @BindView(R.id.tv_date)
    AppCompatTextView tvDate;

    private GoogleMap mMap;
    private double userLat, userLang;
    ReserveHelp reserveHelp;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_map);
        ButterKnife.bind(this);
        init();
        getDataBundle();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        fillData(reserveHelp);

    }

    private void init() {
        progress = Util.progressUtil(this);
    }

    private void getDataBundle() {
        Bundle bundle = getIntent().getExtras();
        reserveHelp = (ReserveHelp) bundle.getSerializable(RESERVE_HELP_TABLE);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setUserLocation();
    }


    private void setUserLocation() {
        userLat = Double.parseDouble(reserveHelp.getLatitude());
        userLang = Double.parseDouble(reserveHelp.getLongitude());
        LatLng userLocation = new LatLng(userLat, userLang);
        mMap.addMarker(new MarkerOptions().position(userLocation));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 15));
    }

    @OnClick({R.id.iv_pin, R.id.iv_phone, R.id.btn_accept, R.id.btn_cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_pin:

                String url = "http://maps.google.com/maps?f=d&daddr=" + reserveHelp.getLatitude() + "," + reserveHelp.getLongitude() + "&dirflg=d&layer=t";
                String uri = String.format(Locale.ENGLISH, url);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);

                break;
            case R.id.iv_phone:
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + reserveHelp.getPhone()));
                if (callIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(callIntent);
                }
                break;
            case R.id.btn_accept:

                break;
            case R.id.btn_cancel:

                break;
        }
    }


    private void fillData(ReserveHelp reserveHelp) {
//        setUserLocation();
        tvDate.setText(reserveHelp.getDate());
        tvCustomerName.setText(reserveHelp.getNameCustomer());
        tvPhone.setText(reserveHelp.getPhone());
    }

}
