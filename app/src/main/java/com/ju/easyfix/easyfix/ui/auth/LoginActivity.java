package com.ju.easyfix.easyfix.ui.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.data.prefs.PrefsUtils;
import com.ju.easyfix.easyfix.model.user.User;
import com.ju.easyfix.easyfix.ui.admin.main.AdminActivity;
import com.ju.easyfix.easyfix.ui.base.BaseActivity;
import com.ju.easyfix.easyfix.ui.user.main.MainActivity;
import com.ju.easyfix.easyfix.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ju.easyfix.easyfix.constant.FirebaseConstant.ADMIN_ACCOUNT;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.et_email)
    AppCompatEditText etEmail;
    @BindView(R.id.et_password)
    AppCompatEditText etPassword;

    private FirebaseAuth mAuth;
    private ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        init();

    }

    private void init() {
        mAuth = FirebaseAuth.getInstance();
        progress = Util.progressUtil(this);
    }


    private void goToActivity(Class activity) {
        startActivity(new Intent
                (this, activity));
    }

    @OnClick({R.id.btn_login, R.id.btn_sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                if (etEmail.getText().toString().isEmpty() || etPassword.getText().toString().isEmpty()) {
                    Toast.makeText(this, "please fill data", Toast.LENGTH_SHORT).show();
                    return;
                }
                progress.show();

                mAuth.signInWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                        .addOnCompleteListener(this, task -> {
                            progress.dismiss();
                            if (task.isSuccessful()) {

                                User user = new User();
                                user.setId(mAuth.getCurrentUser().getUid());
                                if (etEmail.getText().toString().equalsIgnoreCase(ADMIN_ACCOUNT)) {
                                    PrefsUtils.getInstance().setAdmin(true);
                                    goToActivity(AdminActivity.class);
                                } else {
                                    goToActivity(MainActivity.class);
                                }
                                finish();

                            } else {
                                Toast.makeText(this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();


                            }
                        });

                break;
            case R.id.btn_sign_up:
                startActivity(new Intent(this, SignupActivity.class));
                break;
        }
    }
}


