package com.ju.easyfix.easyfix.ui.admin.car.model;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.admin.CarModel;
import com.ju.easyfix.easyfix.model.admin.CarType;

import java.util.List;

import static com.ju.easyfix.easyfix.constant.FirebaseConstant.COMPANY_TABLE;

public class CarModelAdapter extends RecyclerView.Adapter<CarModelAdapter.ViewHolder> {
    private static final int ROW_CAR_TYPE = R.layout.row_car;
    private List<CarModel> carModelList;
    private Context context;
    private AlertDialog.Builder builder;
    private DatabaseReference mDatabaseReference;
    private CarType carTypes;

    public CarModelAdapter(CarType carTypes) {
        this.carModelList = carTypes.getCarModels();
        this.carTypes = carTypes;
//        this.idCarType = idCarType;
    }

    @NonNull
    @Override
    public CarModelAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @Override
    public void onBindViewHolder(@NonNull CarModelAdapter.ViewHolder holder, int position) {
        holder.tvName.setText(carModelList.get(position).getName());

        holder.itemView.setOnLongClickListener(v -> {
            dialogInput(position);

            return false;
        });
    }


    private void dialogInput(int position) {

        builder = new AlertDialog.Builder(context);
        builder.setTitle("Car Model Add");

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_car_model, null);
        builder.setView(dialogView);

        EditText etCarTypeName = dialogView.findViewById(R.id.et_car_model_name);
//        EditText etCarYear = dialogView.findViewById(R.id.et_car_model_year);

        Button btnOk = dialogView.findViewById(R.id.btn_ok);
        Button btnCancel = dialogView.findViewById(R.id.btn_cancel);
        AlertDialog dialog = builder.create();


        etCarTypeName.setText(carModelList.get(position).getName());
//        etCarYear.setText(carModelList.get(position).getYear());

        btnOk.setOnClickListener(v -> {


            mDatabaseReference = FirebaseDatabase.getInstance().getReference()
                    .child(COMPANY_TABLE).child(carTypes.getId()).child("carModels").child(carModelList.get(position).getId());

            mDatabaseReference.child("name").setValue(etCarTypeName.getText().toString());
//            mDatabaseReference.child("year").setValue(etCarYear.getText().toString());
//            mDatabaseReference.child("image").setValue(etCarTypeName.getText().toString());
//            mDatabaseReference.child("background").setValue(etCarTypeName.getText().toString());
            dialog.dismiss();
        });
        btnCancel.setOnClickListener(v -> {
            dialog.cancel();
        });
        dialog.show();
    }


//    private void dialogInput(int position) {
//        builder = new AlertDialog.Builder(context);
//        builder.setTitle("Car Model Name");
//        final EditText etCarTypeName = new EditText(context);
//        etCarTypeName.setText(carModelList.get(position).getName());
//        builder.setView(etCarTypeName);
//
//        carModelList.get(position);
//        builder.setPositiveButton("OK", (dialog, which) -> {
//            Log.e("hisham idCarType ", carTypes.getId());
//            mDatabaseReference = FirebaseDatabase.getInstance().getReference()
//                    .child(COMPANY_TABLE).child(carTypes.getId()).child("carModels").child(carModelList.get(position).getId()).child("name");
//
//            mDatabaseReference.setValue(etCarTypeName.getText().toString());
//
//
//        });
//        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
//        builder.show();
//    }

    @Override
    public int getItemCount() {
        return carModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return ROW_CAR_TYPE;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;

        public ViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tv_name);

        }
    }


}
