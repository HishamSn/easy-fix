package com.ju.easyfix.easyfix.model;

import android.support.annotation.DrawableRes;

public class ModelRowImage {
    private String title;
    private Integer image;

    public ModelRowImage(String title, @DrawableRes Integer image) {
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getImage() {
        return image;
    }


    public void setImage(@DrawableRes Integer image) {
        this.image = image;
    }
}
