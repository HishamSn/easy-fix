package com.ju.easyfix.easyfix.ui.admin.main;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.ModelRowImage;
import com.ju.easyfix.easyfix.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdminActivity extends BaseActivity {


    @BindView(R.id.rv_main)
    RecyclerView rvMain;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        ButterKnife.bind(this);
        init();
        setUpRecyclerView(getMainList());
    }


    private void init() {
        rvMain = findViewById(R.id.rv_main);
    }

    private void setUpRecyclerView(List<ModelRowImage> mainList) {
        rvMain.setLayoutManager(
                new LinearLayoutManager(this));

        rvMain.setAdapter(new AdminAdapter(mainList));
    }

    @NonNull
    private List<ModelRowImage> getMainList() {
        List<ModelRowImage> mainList = new ArrayList<>();
        mainList.add(new ModelRowImage("Reservation", R.drawable.car_workshop));
        mainList.add(new ModelRowImage("Help Customers", R.drawable.bg_location_car));
        mainList.add(new ModelRowImage("Add Cars", R.drawable.bg_cars));

        return mainList;
    }
}
