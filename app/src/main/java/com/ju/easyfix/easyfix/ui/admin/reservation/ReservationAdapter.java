package com.ju.easyfix.easyfix.ui.admin.reservation;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.Reservation;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ReservationAdapter extends RecyclerView.Adapter<ReservationAdapter.ViewHolder> {
    private static final int ROW_MY_ORDER = R.layout.row_admin_reservation;
    private List<Reservation> myReservationList;
    private Context context;

    public ReservationAdapter(List<Reservation> myReservationList) {
        this.myReservationList = myReservationList;
    }

    @NonNull
    @Override
    public ReservationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @Override
    public void onBindViewHolder(@NonNull ReservationAdapter.ViewHolder holder, int position) {

        Reservation myReservation = myReservationList.get(position);
        holder.tvId.setText("#\t" + myReservation.getIdCarType() + " - " + myReservation.getId());
        holder.tvCarType.setText(myReservation.getCarType());
        holder.tvCarModel.setText(myReservation.getCarModel());
        holder.tvCarYear.setText(myReservation.getCarYear());
        holder.tvPlateNumber.setText("566561");
        holder.tvDate.setText(myReservation.getDate());
        holder.tvTime.setText(myReservation.getTime());
        holder.tvCustomerName.setText(myReservation.getNameCustomer());
        holder.tvCustomerPhone.setText(myReservation.getPhone());
        Picasso.with(context).load(myReservation.getImage()).into(holder.ivCarImage);
        holder.itemView.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Reason of Reservation");
            builder.setMessage(myReservation.getDescription());
            AlertDialog alert = builder.create();
            alert.show();
        });

    }


    @Override
    public int getItemCount() {
        return myReservationList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return ROW_MY_ORDER;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvId;
        private TextView tvDate;
        private TextView tvTime;
        private TextView tvCarType;
        private TextView tvCarModel;
        private TextView tvCarYear;
        private TextView tvPlateNumber;
        private TextView tvCustomerName;
        private TextView tvCustomerPhone;
        private ImageView ivCarImage;


        public ViewHolder(View view) {
            super(view);
            tvId = view.findViewById(R.id.tv_reserve_id);
            tvDate = view.findViewById(R.id.tv_date);
            tvTime = view.findViewById(R.id.tv_date);
            tvCarType = view.findViewById(R.id.tv_car_type);
            tvCarModel = view.findViewById(R.id.tv_car_model);
            tvCarYear = view.findViewById(R.id.tv_car_year);
            tvPlateNumber = view.findViewById(R.id.tv_plate_number);
            tvCustomerPhone = view.findViewById(R.id.tv_phone_number);
            tvCustomerName = view.findViewById(R.id.tv_customer_name);
            tvPlateNumber = view.findViewById(R.id.tv_plate_number);
            ivCarImage = view.findViewById(R.id.iv_car);

        }
    }


}
