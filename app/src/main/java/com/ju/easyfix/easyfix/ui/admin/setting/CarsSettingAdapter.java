package com.ju.easyfix.easyfix.ui.admin.setting;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.ModelRowImage;
import com.ju.easyfix.easyfix.ui.admin.car.model.AddCarModelActivity;
import com.ju.easyfix.easyfix.ui.admin.car.type.AddCarTypeActivity;

import java.util.ArrayList;
import java.util.List;

public class CarsSettingAdapter extends RecyclerView.Adapter<CarsSettingAdapter.ViewHolder> {

    private List<ModelRowImage> mainList = new ArrayList<>();
    private AppCompatActivity parentActivity;

    public CarsSettingAdapter(List<ModelRowImage> mainList) {
        this.mainList = mainList;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        parentActivity = (AppCompatActivity) recyclerView.getContext();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image, parent, false);

        view.setLayoutParams(new RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                parent.getMeasuredHeight() / 2
        ));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.bgImageView.setImageResource(mainList.get(position).getImage());
        holder.tvTitle.setText(mainList.get(position).getTitle());


        holder.itemView.setOnClickListener(v -> {
            switch (position) {
                case 0:
                    goToActivity(AddCarTypeActivity.class);
                    break;
                case 1:
                    goToActivity(AddCarModelActivity.class);
                    break;
                case 2:
                    goToActivity(AddCarTypeActivity.class);
                    break;
            }
        });
    }

    private void goToActivity(Class activity) {
        parentActivity.startActivity(new Intent
                (parentActivity, activity));
    }

    @Override
    public int getItemCount() {
        return mainList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView bgImageView;
        AppCompatTextView tvTitle;

        ViewHolder(View v) {
            super(v);
            bgImageView = v.findViewById(R.id.iv_main);
            tvTitle = v.findViewById(R.id.tv_title);
        }
    }
}
