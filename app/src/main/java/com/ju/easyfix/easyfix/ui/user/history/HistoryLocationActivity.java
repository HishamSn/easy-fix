package com.ju.easyfix.easyfix.ui.user.history;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.ReserveHelp;
import com.ju.easyfix.easyfix.model.admin.CarType;
import com.ju.easyfix.easyfix.ui.base.BaseActivity;
import com.ju.easyfix.easyfix.util.SessionUtils;
import com.ju.easyfix.easyfix.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ju.easyfix.easyfix.constant.FirebaseConstant.COMPANY_TABLE;

public class HistoryLocationActivity extends BaseActivity {

    @BindView(R.id.rv_history)
    RecyclerView rvHistoryLocation;
    private DatabaseReference databaseReference;
    private Context context = this;
    private List<ReserveHelp> reserveHelpList;
    private ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        init();
        setAdapter();
        getReservation();
    }

    private void getReservation() {
        progress.show();
        databaseReference = databaseReference.child(COMPANY_TABLE);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progress.dismiss();
                reserveHelpList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    CarType carType = child.getValue(CarType.class);
                    if (carType.getReserveHelp() != null && !carType.getReserveHelp().isEmpty()) {
                        for (int i = 0; i < carType.getReserveHelp().size(); i++) {
                            if (carType.getReserveHelp().get(i).getIdCustomerName()
                                    .equals(SessionUtils.getInstance().mAuth().getUid())) {
                                reserveHelpList.add(carType.getReserveHelp().get(i));
                            }
                        }
                    }
                }
                rvHistoryLocation.setAdapter(new HistoryLocationAdapter(reserveHelpList));

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progress.dismiss();
                Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setAdapter() {

        rvHistoryLocation.setLayoutManager(new LinearLayoutManager(this));
        rvHistoryLocation.setAdapter(new HistoryLocationAdapter(reserveHelpList));
    }

    private void init() {
        progress = Util.progressUtil(this);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        reserveHelpList = new ArrayList<>();

    }
}
