package com.ju.easyfix.easyfix.ui.admin.help;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.ReserveHelp;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.ju.easyfix.easyfix.constant.FirebaseConstant.RESERVE_HELP_TABLE;

public class ReserveHelpAdapter extends RecyclerView.Adapter<ReserveHelpAdapter.ViewHolder> {
    private static final int ROW_MY_ORDER = R.layout.row_admin_reservation;
    private List<ReserveHelp> reserveHelpList;
    private Context context;

    public ReserveHelpAdapter(List<ReserveHelp> reserveHelpList) {
        this.reserveHelpList = reserveHelpList;
    }

    @NonNull
    @Override
    public ReserveHelpAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @Override
    public void onBindViewHolder(@NonNull ReserveHelpAdapter.ViewHolder holder, int position) {

        ReserveHelp reserveHelp = reserveHelpList.get(position);
        holder.tvId.setText("#\t" + reserveHelp.getIdCarType() + " - " + reserveHelp.getId());
        holder.tvCarType.setText(reserveHelp.getCarType());
        holder.tvCarModel.setText(reserveHelp.getCarModel());
        holder.tvCarYear.setText(reserveHelp.getCarYear());
        holder.tvPlateNumber.setText("566561");
        holder.tvDate.setText(reserveHelp.getDate());
        holder.tvTime.setText(reserveHelp.getTime());
        holder.tvCustomerName.setText(reserveHelp.getNameCustomer());
        holder.tvCustomerPhone.setText(reserveHelp.getPhone());
        Picasso.with(context).load(reserveHelp.getImage()).into(holder.ivCarImage);
        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, HelpDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(RESERVE_HELP_TABLE, reserveHelp);
            intent.putExtras(bundle);
            context.startActivity(intent);
        });

    }


    @Override
    public int getItemCount() {
        return reserveHelpList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return ROW_MY_ORDER;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvId;
        private TextView tvDate;
        private TextView tvTime;
        private TextView tvCarType;
        private TextView tvCarModel;
        private TextView tvCarYear;
        private TextView tvPlateNumber;
        private TextView tvCustomerName;
        private TextView tvCustomerPhone;
        private ImageView ivCarImage;


        public ViewHolder(View view) {
            super(view);
            tvId = view.findViewById(R.id.tv_reserve_id);
            tvDate = view.findViewById(R.id.tv_date);
            tvTime = view.findViewById(R.id.tv_date);
            tvCarType = view.findViewById(R.id.tv_car_type);
            tvCarModel = view.findViewById(R.id.tv_car_model);
            tvCarYear = view.findViewById(R.id.tv_car_year);
            tvCustomerPhone = view.findViewById(R.id.tv_phone_number);
            tvCustomerName = view.findViewById(R.id.tv_customer_name);
            tvPlateNumber = view.findViewById(R.id.tv_plate_number);
            ivCarImage = view.findViewById(R.id.iv_car);

        }
    }


}
