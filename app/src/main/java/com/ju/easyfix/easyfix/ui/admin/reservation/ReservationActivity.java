package com.ju.easyfix.easyfix.ui.admin.reservation;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.Reservation;
import com.ju.easyfix.easyfix.model.admin.CarType;
import com.ju.easyfix.easyfix.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ju.easyfix.easyfix.constant.FirebaseConstant.COMPANY_TABLE;

public class ReservationActivity extends BaseActivity {

    @BindView(R.id.rv_admin_reservation)
    RecyclerView rvHistory;
    private DatabaseReference databaseReference;
    private Context context = this;
    private List<Reservation> myReservationList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_reservation);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);

        init();
        setAdapter();
        getReservation();
    }

    private void getReservation() {
        databaseReference = databaseReference.child(COMPANY_TABLE);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                myReservationList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    CarType carType = child.getValue(CarType.class);
                    if (carType.getReservation() != null && !carType.getReservation().isEmpty()) {
                        for (int i = 0; i < carType.getReservation().size(); i++) {
                                myReservationList.add(carType.getReservation().get(i));
                        }
                    }
                }
                rvHistory.setAdapter(new ReservationAdapter(myReservationList));

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setAdapter() {

        rvHistory.setLayoutManager(new LinearLayoutManager(this));
        rvHistory.setAdapter(new ReservationAdapter(myReservationList));
    }

    private void init() {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        myReservationList = new ArrayList<>();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);

    }
}
