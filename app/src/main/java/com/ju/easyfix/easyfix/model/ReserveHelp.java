package com.ju.easyfix.easyfix.model;

import java.io.Serializable;

public class ReserveHelp implements Serializable {

    private String id;
    private String idCarType;
    private String idCarModel;
    private String idCustomerName;
    private String idCustomerCar;
    private String date;
    private String time;
    private String nameCustomer;
    private String carType;
    private String carModel;
    private String carYear;
    private String plateNumber;
    private String image;
    private String phone;
    private String latitude;
    private String longitude;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCarType() {
        return idCarType;
    }

    public void setIdCarType(String idCarType) {
        this.idCarType = idCarType;
    }

    public String getIdCarModel() {
        return idCarModel;
    }

    public void setIdCarModel(String idCarModel) {
        this.idCarModel = idCarModel;
    }

    public String getIdCustomerName() {
        return idCustomerName;
    }

    public void setIdCustomerName(String idCustomerName) {
        this.idCustomerName = idCustomerName;
    }

    public String getIdCustomerCar() {
        return idCustomerCar;
    }

    public void setIdCustomerCar(String idCustomerCar) {
        this.idCustomerCar = idCustomerCar;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNameCustomer() {
        return nameCustomer;
    }

    public void setNameCustomer(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarYear() {
        return carYear;
    }

    public void setCarYear(String carYear) {
        this.carYear = carYear;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
