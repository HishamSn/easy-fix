package com.ju.easyfix.easyfix.ui.admin.car.type;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.easyfix.easyfix.R;
import com.ju.easyfix.easyfix.model.admin.CarModel;
import com.ju.easyfix.easyfix.model.admin.CarType;
import com.ju.easyfix.easyfix.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ju.easyfix.easyfix.constant.FirebaseConstant.COMPANY_TABLE;

public class AddCarTypeActivity extends AppCompatActivity  {
    @BindView(R.id.rv_add_car)
    RecyclerView rvAddCar;

    List<CarType> carTypes = new ArrayList<>();
    private DatabaseReference mDatabaseReference;

    private String carTypeName;
    private AlertDialog.Builder builder;
    private ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_car_type_activity);
        ButterKnife.bind(this);
        init();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        initRecyclerView(layoutManager);

        getCarType();
    }

    private void initRecyclerView(LinearLayoutManager layoutManager) {
        rvAddCar.setLayoutManager(layoutManager);
        rvAddCar.addItemDecoration(new DividerItemDecoration(rvAddCar.getContext(),
                layoutManager.getOrientation()));
        rvAddCar.setAdapter(new CarTypeAdapter(carTypes));
    }

    private void init() {
        progress = Util.progressUtil(this);

        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child(COMPANY_TABLE);
    }


    private void getCarType() {
        progress.show();
        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progress.dismiss();
                carTypes.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    CarType carType = child.getValue(CarType.class);
                    carType.setId(child.getKey());
                    carTypes.add(carType);
                }
                rvAddCar.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progress.dismiss();

            }
        });
    }

    @OnClick(R.id.fab_add_car_type)
    public void onViewClicked() {
        dialogInput();

    }


    private void dialogInput() {

        builder = new AlertDialog.Builder(this);
        builder.setTitle("Car Type Name");
        final EditText etCarTypeName = new EditText(this);
        builder.setView(etCarTypeName);


        builder.setPositiveButton("OK", (dialog, which) -> {
            List<CarModel> carModelsList = new ArrayList<>();
            carTypes.add(new CarType(etCarTypeName.getText().toString(), carModelsList));
            mDatabaseReference.setValue(carTypes);

        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();
    }
}
